package kz.attractorschool.moviereviewrr.util;

import kz.attractorschool.moviereviewrr.model.User;
import kz.attractorschool.moviereviewrr.repository.MovieRepository;
import kz.attractorschool.moviereviewrr.repository.ReviewRepository;
import kz.attractorschool.moviereviewrr.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

@Configuration
public class Test {
    @Autowired
    static MovieRepository mr;
    @Autowired
    static ReviewRepository rr;
    @Autowired
    static UserRepository ur;
    public Test(MovieRepository mr, ReviewRepository rr, UserRepository ur){
        this.mr = mr;
        this.rr = rr;
        this.ur = ur;
        test();
    }
    public void test(){
        Sort s = Sort.by(Sort.Order.asc("title"));
        System.out.println("--------All Movies---------");
        System.out.println(mr.findAll(s));
        System.out.println("---------All Reviews---------------");
        System.out.println(rr.findAll());
        System.out.println("--------------All Users--------------");
        System.out.println(ur.findAll());

//        System.out.println(mr.findAllByDirectorsContains("Anthony Russo"));

    }
}
